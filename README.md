# DarkRift2 using Unity ECS and [BovineLabs EventSystem](https://gitlab.com/tertle/com.bovinelabs.event)

---

### This is a simple example of how you can use DarkRift2 with Unity ECS. It also includes an example usage of sending a message from an ECS System to a DarkRift Client, then to a server and back again.

### This example sends a network message using DarkRift for every entity created in 5 second intervals (this interval is arbitrary).

![](https://gyazo.com/2eb85fd7cee215f029ba4ed713893de5.gif)

### Although the files associated with the example above are in the "Sample" folder, some if its functionality can be found in the other files.

---

## Walkthrough

### Sending messages from ECS

1.  Make a struct for your data that inherits from `INetworkMessageDR`
2.  Use `HybridNetworkMethods.MakeOutboundNetworkMessage([your message type], [an EventSystem writer])` to write to the EventSystem
3.  Go to `NetworkEventSystem.cs` and add your message Type to the `CheckOutboundMessageJob` at the bottom

### Receiving messages back into ECS

1. In ServerMessageManager add your type to the switch statement in `Client_MessageReceived()` using `HybridNetworkMethods.DeserialiseReceivedMessage`
2. Ensure you have a System checking for that type from the EventSystem, see `TestSystem.UpdateEvents()` for an example.

### Key bits

* The DarkRift Client for sending & receiving messages is in the NetworkEventSystem class
* NetworkEventSystem is an ECS System that also checks for messages received & needing to be sent
* Messages go from [Your ECS System] => NetworkEventSystem => Server => NetworkEventSystem => [Your ECS System]
* Always remember to add Producer and Consumer handles when using the EventSystem to avoid hard-to-trace bugs