﻿namespace DarkECS_Hybrid
{
    public enum NetMsgTags : ushort
    {
        SpawnPlayerTag = 0,
        TestTag = 1
    }
}