﻿namespace DarkECS_Hybrid
{
    public enum NetworkCommands : ushort
    {
        Connect,
        Disconnect,
        LoginPF
    }
}
