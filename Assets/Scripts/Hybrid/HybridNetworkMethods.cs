﻿using BovineLabs.Event.Containers;
using BovineLabs.Event.Systems;
using Unity.Jobs;

namespace DarkECS_Hybrid
{
    public class HybridNetworkMethods
    {

        /// <summary>
        /// Write a DarkRift Message to the EventSystem to be sent to the server
        /// </summary>
        /// <typeparam name="MessageType"></typeparam>
        /// <param name="msg">The INetworkMessageDR message, inherits from IDarkRiftSerializable</param>
        /// <param name="writer">The EventSystem writer for OutboundMessageTag</param>
        /// <param name="dependency">Dependency</param>
        public static void MakeOutboundNetworkMessage<MessageType>(MessageType msg, NativeEventStream.Writer writer, JobHandle dependency = default) where MessageType : struct, INetworkMessageDR
        {
            writer.Write(new OutboundMessageTag { tag = msg.Tag });
            writer.Write(msg);
        }

        /// <summary>
        /// Takes a DarkRift message and sends it through the EventSystem
        /// </summary>
        /// <typeparam name="MessageType"></typeparam>
        /// <param name="e"></param>
        /// <param name="eventSystem"></param>
        public static void DeserialiseReceivedMessage<MessageType>(DarkRift.Client.MessageReceivedEventArgs e, EventSystem eventSystem)
            where MessageType : struct,
            INetworkMessageDR
        {
            var _writer = eventSystem.CreateEventWriter<MessageType>();
            eventSystem.AddJobHandleForProducer<MessageType>(default);
            _writer.Write(e.GetMessage().Deserialize<MessageType>());
        }
    }
}