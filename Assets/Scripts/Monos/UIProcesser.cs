﻿using BovineLabs.Event.Systems;
using DarkECS_sample;
using System;
using System.Diagnostics;
using System.Net;
using System.Text;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace DarkECS_Hybrid
{
    public class UIProcesser : MonoBehaviour
    {
        public InputField inpIPAddress;
        public InputField inpPort;
        public InputField inpEntityText;
        public Text txtLog;

        private EntityManager entityManager;
        private EventSystem eventSystem;

        // Start is called before the first frame update
        void Start()
        {
            entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            eventSystem = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<EventSystem>();
            if (inpEntityText == null || inpPort == null || inpIPAddress == null || txtLog == null)
                throw new Exception("unasigned UI elements in UIProcessor.cs, go into the editor and drag and drop the content onto the fields of this script.");
        }

        public void ConnectToServer()
        {
            if (IPAddress.TryParse(inpIPAddress.text, out IPAddress _ip))
            {
                if (ushort.TryParse(inpPort.text, out ushort _port))
                {
                    var _msgWriter = eventSystem.CreateEventWriter<NetworkCommand>();
                    _msgWriter.Write(new NetworkCommand(NetworkCommands.Connect));
                    foreach(byte _byte in _ip.GetAddressBytes())
                    {
                        _msgWriter.Write(_byte);
                    }
                    _msgWriter.Write(_port);
                    eventSystem.AddJobHandleForProducer<NetworkCommand>(default);
                }
            }

            
        }

        public void DisconnectFromServer()
        {
            var _msgWriter = eventSystem.CreateEventWriter<NetworkCommand>();
            _msgWriter.Write(new NetworkCommand(NetworkCommands.Disconnect));
            eventSystem.AddJobHandleForProducer<NetworkCommand>(default);
        }

        public void LoginPlayFab()
        {
            var _msgWriter = eventSystem.CreateEventWriter<NetworkCommand>();
            _msgWriter.Write(new NetworkCommand(NetworkCommands.LoginPF));
            eventSystem.AddJobHandleForProducer<NetworkCommand>(default);
        }

        // SAMPLE METHODS

        /// <summary>
        /// Create an entity with a TestComponent and use the text value from the UI
        /// </summary>
        public void CreateEntity()
        {
            Entity _entity = entityManager.CreateEntity(typeof(TestComponent));
            if (inpEntityText.text.Length > 0)
                entityManager.SetComponentData(_entity, new TestComponent() { TestValue = inpEntityText.text });
        }

        /// <summary>
        /// Update the UI text log with the string "newInfo"
        /// </summary>
        /// <param name="newInfo">The string to add the UI log</param>
        public void UpdateLog(FixedString64 newInfo)
        {
            var _strbld = new StringBuilder("\n");
            _strbld.Append("Received message: ");
            _strbld.Append(newInfo);
            txtLog.text += _strbld.ToString();
        }
    }
}
