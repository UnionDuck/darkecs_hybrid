﻿using DarkECS_Hybrid;
using DarkRift;
using DarkRift.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkECS_Hybrid_Server
{
    public class TestPlugin : Plugin
    {
        public override bool ThreadSafe => false;
        public override Version Version => new Version(1, 0, 0);

        private readonly Dictionary<IClient, Player> Players = new Dictionary<IClient, Player>();

        public TestPlugin(PluginLoadData  pluginLoadData) : base(pluginLoadData)
        {
            ClientManager.ClientConnected += ClientManager_ClientConnected;
            ClientManager.ClientDisconnected += ClientManager_ClientDisconnected;
        }

        private void ClientManager_ClientDisconnected(object sender, ClientDisconnectedEventArgs e)
        {
            Player _newPlayer = new Player(e.Client.ID);
            //Update other players on new player info
            using (DarkRiftWriter newPlayerWriter = DarkRiftWriter.Create())
            {
                newPlayerWriter.Write(_newPlayer.ID);
                using (Message newPlayerMessage = Message.Create((ushort)NetMsgTags.SpawnPlayerTag, newPlayerWriter))
                {
                    foreach (IClient client in ClientManager.GetAllClients().Where(x => x != e.Client))
                        client.SendMessage(newPlayerMessage, SendMode.Reliable);
                }
            }

            Players.Add(e.Client, _newPlayer);

            //Tell new player about existing players
            using (DarkRiftWriter playersWriter = DarkRiftWriter.Create())
            {
                foreach (Player player in Players.Values)
                {
                    playersWriter.Write(player.ID);
                }

                using (Message playersMessage = Message.Create((ushort)NetMsgTags.SpawnPlayerTag, playersWriter))
                    e.Client.SendMessage(playersMessage, SendMode.Reliable);
            }
        }

        private void ClientManager_ClientConnected(object sender, ClientConnectedEventArgs e)
        {
            e.Client.MessageReceived += Client_MessageReceived;
        }

        private void Client_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            using (Message msg = e.GetMessage())
            {
                switch (msg.Tag)
                {
                    case (ushort)NetMsgTags.TestTag:
                        TestMsgRecieved(sender, e);
                        break;

                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Logs the message content to the console then sends the message back to all clients
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TestMsgRecieved(object sender, MessageReceivedEventArgs e)
        {
            using (Message msg = e.GetMessage())
            {
                foreach (Player player in Players.Values)
                {
                    if (player.ID == e.Client.ID)
                    {
                        using (DarkRiftReader reader = msg.GetReader())
                        {
                            player.TestStr = reader.ReadString();
                            Logger.Info(player.TestStr);
                        }
                    }
                }
                //send this message to all clients
                foreach (IClient client in ClientManager.GetAllClients())
                    client.SendMessage(e.GetMessage(), SendMode.Reliable);
            }
        }
    }
}
