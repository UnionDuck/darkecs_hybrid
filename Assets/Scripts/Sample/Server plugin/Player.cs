﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkECS_Hybrid_Server
{
    class Player
    {
        public ushort ID { get; set; }
        public string TestStr { get; set; }

        public Player(ushort ID)
        {
            this.ID = ID;
            TestStr = "Default";
        }
    }
}
