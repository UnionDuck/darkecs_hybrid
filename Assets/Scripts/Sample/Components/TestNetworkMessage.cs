﻿using DarkECS_Hybrid;
using DarkRift;
using Unity.Collections;

namespace DarkECS_sample
{
    /// <summary>
    /// A NetworkEvent (meaning it goes through the EventSystem to be sent to the server 
    /// that contains a string and its Tag 
    /// </summary>
    public struct TestNetworkMessage : INetworkMessageDR
    {
        public FixedString64 testString;
        public ushort Tag { get; set; }

        public void Deserialize(DeserializeEvent e)
        {
            testString = e.Reader.ReadString();
            Tag = e.Reader.ReadUInt16();
        }

        public void Serialize(SerializeEvent e)
        {
            e.Writer.Write(testString.ToString());
            e.Writer.Write(Tag);
        }
    }
}
