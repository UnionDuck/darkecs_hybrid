﻿using DarkECS_Hybrid;
using Unity.Collections;
using Unity.Entities;

namespace DarkECS_sample
{
    /// <summary>
    /// A sample component with a NativeString field and a method for making a NetworkMessage type
    /// </summary>
    public struct TestComponent : IComponentData
    {
        public FixedString64 TestValue;

        public TestNetworkMessage ToNetworkMessage()
        {
            var _returnData = new TestNetworkMessage
            {
                testString = TestValue
            };
            return _returnData;
        }
    }
}
