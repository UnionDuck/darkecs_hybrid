﻿using BovineLabs.Event.Containers;
using BovineLabs.Event.Jobs;
using BovineLabs.Event.Systems;
using DarkECS_Hybrid;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace DarkECS_sample
{
    /// <summary>
    /// A sample system that waits a specific amount of time ("tick") and then
    /// gets all of the entities with the "TestComponent" and sends their content
    /// through the EventSystem to be sent as NetworkEvents to the server, then deletes 
    /// the entitites.
    /// </summary>
    [AlwaysUpdateSystem]
    public class TestSystem : SystemBase
    {
        // uses "AlwaysUpdateSystem" to ensure EventSystem is checked even when no TestComponents exist
        private const float tick = 5f;
        private float elapsed = 0.0f;
        private EndSimulationEntityCommandBufferSystem endSimulationEntityCommandBufferSystem;
        private EventSystem eventSystem;
        private UIProcesser uiProcesser;

        protected override void OnCreate()
        {
            endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            eventSystem = World.GetOrCreateSystem<EventSystem>();

            base.OnCreate();
        }

        protected override void OnStartRunning()
        {
            //need a reference for the input manager in order update the UI with received msgs
            uiProcesser = GameObject.Find("InputManager").GetComponent<UIProcesser>();
        }

        protected override void OnUpdate()
        {
            elapsed += Time.DeltaTime;

            if (elapsed > tick)
            {
                // Create a writer for OutboundMessageTag events
                var _msgWriter = eventSystem.CreateEventWriter<OutboundMessageTag>();
                //get an entity command buffer for queueing up DestroyEntity() commands
                var _entitycmdbuf = endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();

                //go through all entities with a TestComponent and send an EventSystem message for each one to get it queued as a network event
                Entities
                    .WithoutBurst()
                    .ForEach((Entity entity, int entityInQueryIndex, ref TestComponent test) =>
                {
                    HybridNetworkMethods.MakeOutboundNetworkMessage(
                        new TestNetworkMessage() { Tag = (ushort)NetMsgTags.TestTag, testString = test.TestValue },
                        _msgWriter);

                    //queue up the destruction of this entity
                    _entitycmdbuf.DestroyEntity(entityInQueryIndex, entity);

                }).Schedule();

                //ensure handles are assigned
                eventSystem.AddJobHandleForProducer<OutboundMessageTag>(this.Dependency);
                endSimulationEntityCommandBufferSystem.AddJobHandleForProducer(this.Dependency);

                elapsed = 0;
            }

            UpdateEvents();

        }

        /// <summary>
        /// Check for messages from the server in the EventSystem, and update the UI log with 
        /// any received messages
        /// </summary>
        void UpdateEvents()
        {
            NativeList<FixedString64> results = new NativeList<FixedString64>(Allocator.TempJob);

            var dep = new TestEventJob
            {
                results = results
            }
            .Schedule<TestEventJob, TestNetworkMessage>(eventSystem);

            dep.Complete();

            if (results.Length > 0)
            {
                foreach (FixedString64 str in results)
                {
                    uiProcesser.UpdateLog(str);
                }
            }
            results.Dispose();
        }
    }

    /// <summary>
    /// Checks for TestNetworkMessages and writes the data to a NativeList of NativeString64
    /// </summary>
    [BurstCompile]
    public struct TestEventJob : IJobEventReader<TestNetworkMessage>
    {
        public NativeList<FixedString64> results;

        public void Execute(NativeEventStream.Reader reader, int readerIndex)
        {
            for (var foreachIndex = 0; foreachIndex < reader.ForEachCount; foreachIndex++)
            {
                var count = reader.BeginForEachIndex(foreachIndex);
                for (var i = 0; i < count; i++)
                {
                    results.Add(reader.Read<TestNetworkMessage>().testString);
                }
            }
        }
    }
}
