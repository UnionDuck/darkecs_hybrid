﻿using BovineLabs.Event.Containers;
using BovineLabs.Event.Jobs;
using BovineLabs.Event.Systems;
using DarkECS_Hybrid;
using DarkECS_sample;
using DarkRift;
using DarkRift.Client;
using DarkRift.Dispatching;
using System;
using System.Collections.Generic;
using System.Net;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using System.Linq;

namespace DarkECS_Hybrid
{
    /// <summary>
    /// The system that checks the EventSystem for "OutboundMessageTag"s 
    /// and queues the following data to be sent to the server
    /// </summary>
    public class NetworkEventSystem : SystemBase
    {
        private ClientObjectCacheSettings ObjectCacheSettings;
        private readonly SerializableObjectCacheSettings objectCacheSettings = new SerializableObjectCacheSettings();
        
        private DarkRiftClient darkriftClient;
        private Dispatcher dispatcher;
        private EventSystem eventSystem;

        private readonly bool invokeFromDispatcher = true;
        private readonly bool sniffData = false;

        private static readonly List<INetworkMessageDR> QueuedMessages = new List<INetworkMessageDR>();

        /// <summary>
        /// Called only once when system is first created & before first OnUpdate()
        /// </summary>
        protected override void OnCreate()
        {
            EntityManager.CreateArchetype(typeof(OutboundMessageTag));
            ObjectCacheSettings = objectCacheSettings.ToClientObjectCacheSettings();
            darkriftClient = new DarkRiftClient(ObjectCacheSettings);
            dispatcher = new Dispatcher(true);

            eventSystem = World.GetOrCreateSystem<EventSystem>();

            darkriftClient.MessageReceived += DarkriftClient_MessageReceived;
            darkriftClient.Disconnected += DarkriftClient_Disconnected;

            base.OnCreate();
        }

        private void DarkriftClient_Disconnected(object sender, DisconnectedEventArgs e)
        {
            if (!e.LocalDisconnect)
                Debug.Log("Disconnected from server, error: " + e.Error);
        }

        private void DarkriftClient_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            if (sniffData)
                Debug.Log("Message Received"); 

            switch ((NetMsgTags)e.Tag)
            {
                case NetMsgTags.TestTag:
                    using (DarkRiftReader reader = e.GetMessage().GetReader())
                    {                       
                        HybridNetworkMethods.DeserialiseReceivedMessage<TestNetworkMessage>(e, eventSystem);
                    }
                    break;
            }
        }

        public void LoginPlayFab()
        {
            var request = new LoginWithCustomIDRequest { CustomId = UnityEngine.Random.value.ToString(), CreateAccount = true };
            PlayFabClientAPI.LoginWithCustomID (request, OnLoginSuccess, OnLoginFailure);
        }

        private void OnLoginFailure(PlayFabError obj)
        {
            Debug.LogError(obj.ErrorMessage);
        }

        private void OnLoginSuccess(LoginResult obj)
        {
            Debug.Log("Successfully logged in as " +  obj.PlayFabId);
        }

        #region UnityClient methods

        /// <summary>
        ///     Connects to a remote server.
        /// </summary>
        /// <param name="ip">The IP address of the server.</param>
        /// <param name="port">The port of the server.</param>
        /// <param name="noDelay">Whether to disable Nagel's algorithm or not.</param>
        public void Connect(IPAddress ip, int port, bool noDelay)
        {
            try 
            { 
                darkriftClient.Connect(ip, port, noDelay);
            }
            catch (Exception e) { Debug.LogError(e.Message); }

            //darkriftClient.ConnectInBackground(IP, port, noDelay);

            if (darkriftClient.ConnectionState == ConnectionState.Connected)
                Debug.Log("Connected to " + ip + " on port " + port + ".");
            else
                Debug.Log("Connection failed to " + ip + " on port " + port + ".");
        }

        /// <summary>
        ///     Connects to a remote server.
        /// </summary>
        /// <param name="ip">The IP address of the server.</param>
        /// <param name="tcpPort">The port the server is listening on for TCP.</param>
        /// <param name="udpPort">The port the server is listening on for UDP.</param>
        /// <param name="noDelay">Whether to disable Nagel's algorithm or not.</param>
        public void Connect(IPAddress ip, int tcpPort, int udpPort, bool noDelay)
        {
            darkriftClient.Connect(ip, tcpPort, udpPort, noDelay);

            if (darkriftClient.ConnectionState== ConnectionState.Connected)
                Debug.Log("Connected to " + ip + " on port " + tcpPort + "|" + udpPort + ".");
            else
                Debug.Log("Connection failed to " + ip + " on port " + tcpPort + "|" + udpPort + ".");
        }

        /// <summary>
        ///     Connects to a remote asynchronously.
        /// </summary>
        /// <param name="ip">The IP address of the server.</param>
        /// <param name="port">The port of the server.</param>
        /// <param name="noDelay">Whether to disable Nagel's algorithm or not.</param>
        /// <param name="callback">The callback to make when the connection attempt completes.</param>
        public void ConnectInBackground(IPAddress ip, int port, bool noDelay, DarkRiftClient.ConnectCompleteHandler callback = null)
        {
            darkriftClient.ConnectInBackground(
                ip,
                port,
                noDelay,
                delegate (Exception e)
                {
                    if (callback != null)
                    {
                        if (invokeFromDispatcher)
                            dispatcher.InvokeAsync(() => callback(e));
                        else
                            callback.Invoke(e);
                    }

                    if (darkriftClient.ConnectionState == ConnectionState.Connected)
                        Debug.Log("Connected to " + ip + " on port " + port + ".");
                    else
                        Debug.Log("Connection failed to " + ip + " on port " + port + ".");
                }
            );
        }

        /// <summary>
        ///     Connects to a remote asynchronously.
        /// </summary>
        /// <param name="ip">The IP address of the server.</param>
        /// <param name="tcpPort">The port the server is listening on for TCP.</param>
        /// <param name="udpPort">The port the server is listening on for UDP.</param>
        /// <param name="noDelay">Whether to disable Nagel's algorithm or not.</param>
        /// <param name="callback">The callback to make when the connection attempt completes.</param>
        public void ConnectInBackground(IPAddress ip, int tcpPort, int udpPort, bool noDelay, DarkRiftClient.ConnectCompleteHandler callback = null)
        {
            darkriftClient.ConnectInBackground(
                ip,
                tcpPort,
                udpPort,
                noDelay,
                delegate (Exception e)
                {
                    if (callback != null)
                    {
                        if (invokeFromDispatcher)
                            dispatcher.InvokeAsync(() => callback(e));
                        else
                            callback.Invoke(e);
                    }

                    if (darkriftClient.ConnectionState == ConnectionState.Connected)
                        Debug.Log("Connected to " + ip + " on port " + tcpPort + "|" + udpPort + ".");
                    else
                        Debug.Log("Connection failed to " + ip + " on port " + tcpPort + "|" + udpPort + ".");
                }
            );
        }

        private bool Disconnect()
        {
            try
            {
                if(darkriftClient.ConnectionState == ConnectionState.Connected || 
                    darkriftClient.ConnectionState == ConnectionState.Connecting)
                    darkriftClient.Disconnect();
            }
            catch (Exception e) { Debug.LogError(e.Message); return false; }
            return true;
        }

        #endregion

        private void SendMessages()
        {
            if (darkriftClient.ConnectionState == ConnectionState.Connected)
            {
                foreach (INetworkMessageDR msg in QueuedMessages)
                {
                    using (DarkRiftWriter writer = DarkRiftWriter.Create())
                    {
                        if (msg != null)
                        {
                            writer.Write(msg);

                            using (Message message = Message.Create(msg.Tag,
                                writer))
                            {
                                darkriftClient.SendMessage(message, SendMode.Reliable);
                            }
                        }
                    }
                }
            }
            QueuedMessages.Clear();
        }

        protected override void OnUpdate()
        {
            NativeList<NetworkCommand> cmds = new NativeList<NetworkCommand>(Allocator.TempJob);
            NativeArray<byte> _ip = new NativeArray<byte>(4, Allocator.TempJob);
            byte _port = 0;

            new CheckNetworkCommandsJob
            {
                returnData = cmds,
                ip = _ip,
                port = _port
            }
            .Schedule<CheckNetworkCommandsJob, NetworkCommand>(eventSystem)
                .Complete();

            if (cmds.Length > 0)
            {
                foreach (NetworkCommand cmd in cmds)
                {
                    switch (cmd.tag)
                    {
                        case NetworkCommands.Connect:
                            Connect(new IPAddress(_ip.ToArray()), _port, false);                          
                            break;
                        case NetworkCommands.Disconnect:
                            Disconnect();
                            break;
                        case NetworkCommands.LoginPF:
                            LoginPlayFab();
                            break;
                        default:
                            break;
                    }
                }
            }
            cmds.Dispose();
            _ip.Dispose();

            //create the job, the EventSystem needs the Job type and the Event listening type passed into the < brackets > 
            this.Dependency = new CheckOutboundMessageJob { }
                .ScheduleParallel<CheckOutboundMessageJob, OutboundMessageTag>(eventSystem);
            Dependency.Complete();

            if (QueuedMessages.Count > 0)
            {
                SendMessages();
            }
        }

        #region Jobs

        public struct CheckNetworkCommandsJob : IJobEventReader<NetworkCommand>
        {
            public NativeList<NetworkCommand> returnData;
            public NativeArray<byte> ip;
            public byte port;

            public void Execute(NativeEventStream.Reader stream, int readerIndex)
            {
                for (var foreachIndex = 0; foreachIndex < stream.ForEachCount; foreachIndex++)
                {
                    var count = stream.BeginForEachIndex(foreachIndex);

                    for (var i = 0; i < count; i++)
                    {
                        returnData.Add(stream.Read<NetworkCommand>());

                        switch (returnData[returnData.Length - 1].tag)
                        {
                            case NetworkCommands.Connect:
                                for (int x = 0; x < 3; x++)
                                {
                                    ip[x] = stream.Read<byte>();
                                    i++;
                                }
                                port = stream.Read<byte>();
                                i++;
                                break;
                            case NetworkCommands.Disconnect:
                                break;
                            case NetworkCommands.LoginPF:
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Checks for the EventSystem for the "OutboundMessageTag" event, reads its "tag" 
        /// field and then reads the next message based upon that tag.
        /// </summary>
        public struct CheckOutboundMessageJob : IJobEventReaderForEach<OutboundMessageTag>
        {
            public void Execute(NativeEventStream.Reader reader, int readerIndex)
            {
                var count = reader.BeginForEachIndex(readerIndex);

                for (var i = 0; i < count; i++)
                {
                    var tag = reader.Read<OutboundMessageTag>().tag;

                    switch ((NetMsgTags)tag)
                    {
                        case NetMsgTags.TestTag:
                            QueuedMessages.Add(reader.Read<TestNetworkMessage>());
                            i++; //increase increment whenever we read so we don't go out of bounds
                            break;
                    }
                }
            }
        }

        #endregion
    }
}
