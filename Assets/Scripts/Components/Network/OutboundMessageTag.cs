﻿using Unity.Entities;

namespace DarkECS_Hybrid
{
    /// <summary>
    /// Used in the EventSystem to signal that a message needs to be sent to the server,
    /// the "tag" component tells the NetworkMessageSystem what type to read next
    /// </summary>
    public struct OutboundMessageTag : IComponentData
    {
        public ushort tag;
    }
}
