﻿using DarkRift;

namespace DarkECS_Hybrid
{
    /// <summary>
    /// Interface inheriting from IDarkRiftSerializable network message data. 
    /// Includes a message 'tag' to identify type.
    /// </summary>
    public interface INetworkMessageDR : IDarkRiftSerializable
    {
        ushort Tag { get; set; }
    }
}
