﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Entities;

namespace DarkECS_Hybrid
{
    public struct NetworkCommand : IComponentData
    {
        public NetworkCommands tag;

        public NetworkCommand(NetworkCommands _tag)
        {
            tag = _tag;
        }
    }
}
